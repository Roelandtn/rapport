---
title: "ACP Municipios Mexico"
output: html_document
---

```{r library}
library(factoextra)
library(corrplot)
library(readOGR)
library(rCarto)

```

```{r readRDS}
tab_freq_pourc_pers_viv <- readRDS("../data/traitees/tab_freq_pourc_pers_viv.rds")
```

Liste des variables retenues pour l'ACP:

*PERTE_INDIGENA_1 [200]-> PERTE_INDIGENA = SI -> Part de la population se considèrant comme indigène
*TENENCIA_1 [161]-> Part des propriétaires
*CLAVIVP_7 [22] -> Local no construido para habitación -> Part de l'habitat précaire ??
*PISOS_2 [27] -> Cemento o firme -> Par des habitations dont le sol est en ciment
*TotPersCuart -> Nombre moyen d'occupant par pièces
*CONACT_10 [2]-> trabajó? -> Part des personnes qui ont un travail
*DESTINO_BASURA_6 [126] -> la tiran en otro lugar? (Calle, baldío, río) -> Part des vivienda dont les déchets sont simplement jetés à l'extérieur de l'habitation
*AGUA_ENTUBADA_3 [89] -> ¿No tienen agua entubada? -> Part des vivienda sans eau courante
*NIVACAD_13 [224] -> Maestría -> Part des personne ayant un niveau de scolarité équivalent au master
*DRENAJE_5 [119] -> ¿No tiene drenaje? -> Part des habitations qui n'ont pas de système d'évacuation des eaux usées

```{r readRDS}
TotPersCuart <- readRDS("../data/traitees/TotPersCuart.rds")
```

```{r merge}
ACP_Data <- merge(TotPersCuart,tab_freq_pourc_pers_viv[,c(1,2,27,22,89,119,126,161,200,224)], by="code_mun")
```

```{r}
ACP_Data_2 <- data.frame(ACP_Data,row.names=ACP_Data$code_mun)
```

```{r}
ACP_Data_3 <- scale(ACP_Data_2[,c(2:11)])
```


```{r princop}
PCR<- princomp(ACP_Data_3, cor = TRUE)
```

```{r get_eigenvalue}
eig.val <- get_eigenvalue(PCR)
eig.val
```

```{r fviz_eig}
fviz_eig(PCR, addlabels = TRUE, ylim = c(0, 50))
```

```{r get_pca_var}
ResultsPCA <- get_pca_var(PCR)
```

```{r corrplot}
corrplot(ResultsPCA$cos2, is.corr=FALSE)
```

```{r fviz_pca_var}
fviz_pca_var(PCR, col.var = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE
             )
```

```{r corrplot}
corrplot(ResultsPCA$contrib, is.corr=FALSE)
```

```{r fviz_contrib}
fviz_contrib(PCR, choice = "var", axes = 1, top = 10)
```

```{r fviz_contrib}
fviz_contrib(PCR, choice = "var", axes = 2, top = 10)
```

```{r fviz_pca_ind}
fviz_pca_ind (PCR, col.ind = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE
             )
```

```{r fviz_pca_ind}
fviz_pca_ind (PCR, col.ind = "contrib",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE
             )
```

```{r fviz_pca_biplot}
fviz_pca_biplot(PCR, repel = TRUE,
                col.var = "#2E9FDF", 
                col.ind = "#696969"  
                )
```

```{r readOGR}
Municipios_2015 <- readOGR(dsn= "../data/areas_geoestadisticas_municipales.shp", layer = "areas_geoestadisticas_municipales",encoding= "utf8", stringsAsFactors = FALSE)

```

```{r proj}
projection <- "+proj=utm +zone=14 +datum=WGS84 +units=m +no_defs + ellps=WGS84"
```

```{r spTransform}
Municipios_2015 <- spTransform(Municipios_2015, projection)
```

```{r paste}
Municipios_2015$ID <- paste(Municipios_2015$CVE_ENT, Municipios_2015$CVE_MUN, sep="")
```

```{r get_pca_ind}
ind <- get_pca_ind(PCR)
```

```{r head}
coord <- head(ind$coord)
```

```{r scores}
PC1 <- PCR$scores[,1] ; round(PC1, 2)
```


```{r cbind}
PC1tab <- cbind(PC1,ACP_Data)
```

```{r}
1+(10/3)*log10(76)
```

```{r }
Discretisation <- c(-7,-3,-2,-1,0,1,2,3,7)

```

```{r }
Discretisation2 <- c(-7,-3,-2,-1)

```

```{r }
Discretisation3 <- c(0,1,2,3,7)

```

```{r length}
Nval <- length(Discretisation2) 
```

```{r length}
Nval2 <- length(Discretisation3)
```

```{r Choropleth}

choroLayer(spdf =  Municipios_2015, spdfid ="ID",
           df =  PC1tab, dfid = "code_mun",
           var =  "PC1",  breaks = Discretisation,
           col= carto.pal(pal1 = "blue.pal", n1 = Nval,pal2 ="red.pal"  ,n2 = Nval2, middle = FALSE, transparency = FALSE),legend.title.txt = "Coordonnées PCA1",legend.pos = "topleft")

layoutLayer(title = "ACP sur les municipios de Mexico: coordonnées de la première composante",
            sources =  "INEGI - Encuesta Intercensal, 2015",postitle = "center", author =  "",scale =0,north = TRUE)

labelLayer(spdf=Municipios_2015, df=PC1tab, spdfid ="ID", dfid ="code_mun", txt="code_mun", col = "black",cex = 0.5)

```