---
title: "test chargement databases"
output: html_notebook
---

# Phase préliminaires

## COntrôle du dossier courant

Cela devrait afficher quelque chose comme:

  "<chemin_vers>/M2_projet_transversal/docs"

```{r}
getwd()
```
## Contrôle de la présence des fichiers

```{r}

list.files("./data/databases")
```

# Chargement des bases
```{r usingffbase}
## source :https://rhandbook.wordpress.com/2013/11/12/ff-ffbase/
library("ffbase")

# définir le dossier où seront stockés les morceaux utilisés par ffbase
# options(fftempdir = "./data/temp/")
options(fftempdir = "/media/win/Users/Nicolas/Documents/mexique/temp")

## découpage en morceaux
morceaux_vivienda <- read.csv.ffdf(file="./data/databases/vivienda.csv", header=T, sep=",", VERBOSE=T, next.rows=500000, colClasses=NA)
morceaux_persona <- read.csv.ffdf(file="./data/databases/persona.csv", header=T, sep=",", VERBOSE=T, next.rows=300000, colClasses=NA)

## fusion des 2 bases

Merged_data = merge(morceaux_persona, morceaux_vivienda, by.x="ID_VIV", by.y="ID_VIV", trace=T)
str(Merged_data)
#View(Merged_data)

nrow(Merged_data)
colnames(Merged_data)
#unique(Merged_data$ID_VIV)

Merged_data2 <- within(Merged_data, {ENTMUN <- paste(ENT.x, MUN.x, sep="")})
colnames(Merged_data2)
```
```{r}

ffdat <- as.ffdf(data.frame(x=1:10, y=10:1))
# add z to the ffdat
within(ffdat, {z <- x+y})
```

## Création d'un id ENT + MUN
 Pas possible avec ffdata (champs numériques uniquement)

## Sauvegarder le ffdf
```{r}
# Création d'un répertoire temporaire
td <- tempfile()

# save the ffdf into the supplied directory
save.ffdf(Merged_data, dir=td)

# what in the directory?
dir(td)

#remove the ffdf from memory
rm("Merged_data")

# and reload the stored ffdf
load.ffdf(dir=td)

tf <- paste(tempfile(), ".zip", sep="")
packed <- pack.ffdf(file=tf, Merged_data)

#remove the ffdf from memory
rm("Merged_data")

# restore the ffdf from the packed ffdf
unpack.ffdf(tf)
```

save.ffdf(pers_viv_ff)
```

# sauvegarde dans un csv de la base complète
```{r }
rep_stockage_csv <- "/media/win/Users/Nicolas/Documents/mexique/pers_viv_mx.csv"
write.csv.ffdf(Merged_data, rep_stockage_csv)

```

```{r}
library('ffbase')
dir("/media/win/Users/Nicolas/Documents/mexique/temp")
load.ffdf('ffdb')
```
## à esssayer

save_ffdf.R : https://github.com/edwindj/ffbase/blob/master/examples/save_ffdf.R

```{r eval = false}
iris.ffdf <- as.ffdf(iris)

td <- tempfile()

# save the ffdf into the supplied directory
save.ffdf(iris.ffdf, dir=td)

# what in the directory?
dir(td)

#remove the ffdf from memory
rm("iris.ffdf")

# and reload the stored ffdf
load.ffdf(dir=td)

tf <- paste(tempfile(), ".zip", sep="")
packed <- pack.ffdf(file=tf, iris.ffdf)

#remove the ffdf from memory
rm("iris.ffdf")

# restore the ffdf from the packed ffdf
unpack.ffdf(tf)
```

# Contrôle du CSV en sortie
## Taille du fichier
```{r}
taille_fichier <-  file.info(rep_stockage_csv)$size


### Lisible par les êtres humains
### source: https://cran.r-project.org/web/packages/gdata/index.html
library("gdata")
humanReadable(taille_fichier, standard="Unix", units="G")
humanReadable(taille_fichier, standard="Unix", units="M")
```
## Lecture des 10 premières lignes 
```{r}
readLines(con = rep_stockage_csv, n = 10)
```
## Contrôle du nombres de lignes

```{python}
## source: https://stackoverflow.com/questions/19001402/how-to-count-the-total-number-of-lines-in-a-text-file-using-python
with open("/media/win/Users/Nicolas/Documents/mexique/pers_viv_mx.csv") as f:
    print sum(1 for _ in f)
```

```{r}
## source: https://www.r-bloggers.com/easy-way-of-determining-number-of-linesrecords-in-a-given-large-file-using-r/

# Ouverture du fichier
testcon <- file(rep_stockage_csv, open="r")

# Lit 20000 lignes à chaque fois (conso mémoire moindre)
readsizeof <- 20000

# stockage du nombre de lignes
nooflines <- 0

# boucle pour parcourir le fichier
( while((linesread <- length(readLines(testcon,readsizeof))) > 0 ) 
nooflines <- nooflines+linesread )
close(testcon)

# Affichage du nombre de lignes
nooflines
```



# Extraction des localités appartenant à la vallée de Mexico

# Concaténation des bases

