---
title: "Analyse bivariée"
author: "Nicolas"
date: "22 décembre 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r chargement des librairies}
library('readr')
library('ggplot2') 
library(ggplot2)
library(dplyr)
library(scales)
library(purrr)
library(knitr)
```

# Test de saveRDS


```{r, eval = FALSE}
VIVIENDA_PERSONA_15 <- read_csv("../data/traitees/persona_vivienda_zm_mexico_2015_concentration.csv", 
    col_types = cols(ABA_AGUA_ENTU = col_character(), 
        ABA_AGUA_NO_ENTU = col_character(), 
        AGUA_ENTUBADA = col_character(), 
        AIRE_ACON = col_character(), AUTOPROP = col_character(), 
        CELULAR = col_character(), CLAVIVP = col_character(), 
        COMBUSTIBLE = col_character(), COMPUTADORA = col_character(), 
        DESTINO_BASURA = col_character(), 
        DRENAJE = col_character(), ELECTRICIDAD = col_character(), 
        ENT = col_character(), HORNO = col_character(), 
        ID_PERSONA = col_character(), ID_VIV = col_character(), 
        INTERNET = col_character(), LAVADORA = col_character(), 
        LOC50K = col_character(), MUN = col_character(), 
        NOM_ENT = col_character(), NOM_LOC = col_character(), 
        NOM_MUN = col_character(), PERTE_INDIGENA = col_character(), 
        PISOS = col_character(), RADIO = col_character(), 
        REFRIGERADOR = col_character(), SERSAN = col_character(), 
        TELEFONO = col_character(), TELEVISOR = col_character(), 
        TELEVISOR_PP = col_character(), TENENCIA = col_character(), 
        USOEXC = col_character()), locale = locale())
```


```{r, eval= FALSE}
saveRDS(VIVIENDA_PERSONA_15,"../data/traitees/2015_vivienda_persona_ZUM.rds", compress = TRUE)
```

```{r, eval=FALSE}
rm(VIVIENDA_PERSONA_15, data)
```
```{r chargement des données brutes (stocks)}
data <- readRDS('../data/traitees/2015_vivienda_persona_ZUM.rds')
libelles_vars <- readRDS('../data/traitees/libelles_variables.rds')
```
# Toutes variables à l'échelle de la ZUM

```{r creation des fonctions}

diag_bars <- function(df,variable,poids,repertoire, labels){
  ## fonction permettant de créer un diagramme en barre de la variable qualitative (factor) passée en argument
  ## nécessite ggplot2, scales packages 
  
  #variable <- which(names(df) == variable)
  figure <- ggplot(df, aes(x = factor(df[[variable]]), fill = factor(df[[variable]]))) +
  geom_bar(aes(weight = df[[poids]])) +
  labs(title = paste(labels[[variable]][['libelle']],variable, sep =' - '), 
       subtitle = "Zone Urbaine de Mexico 2015",
       x = variable, 
       y = "Nombre d'habitations",
       caption = "(données pondérées INEGI Encuesta Intercensal 2015)") +
   scale_y_continuous(labels = comma) +
  theme(plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 12),
        legend.position="right", # position de la légende
        legend.text=element_text(size=5), # taille de police des éléments de légende
        legend.title = element_text(size= 6), # taille de police du titre de légende
        plot.subtitle = element_text(size = 9), # taille de police du sous-titre
        axis.text=element_text(size=8), # taille de police des étiquettes des axes
        axis.title = element_text(size= 10) # taille de police des titres des axes
        ) +
     scale_fill_manual(values=alpha(rainbow(length(labels[[variable]][['etiquettes']])),.75), 
                       name=labels[[variable]][['libelle']],
                       labels= labels[[variable]][['etiquettes']]
                       )+
    geom_text(stat = 'count'
              , aes(label=..count..),
              position = position_dodge(width = 0.9),
              vjust = 0,
              size = 2)
  # sauvegarde dans un fichier, avec la taille de la figure
  ggsave(filename = paste(repertoire,variable,".png", sep = ""), plot = figure,  width = 180, height = 120, units = "mm")
}

diag_histo <- function(df,variable,poids,repertoire, labels){
  ## fonction permettant de créer un histogramme de la variable quanitative passée en argument
  ## nécessite ggplot2, scales packages 
  
  #variable <- which(names(df) == variable)
  figure <- ggplot(df, aes(x = df[[variable]], fill = df[[variable]])) +
  geom_histogram()+#aes(weight = df[[poids]])) +
  geom_vline(aes(xintercept=mean(df[[variable]])), color='blue', size= 0.5)+
  labs(title = paste(labels[[variable]][['libelle']],variable, sep =' - '), 
       subtitle = "Zone Urbaine de Mexico 2015",
       x = variable, 
       y = "Nombre d'habitations",
       caption = "(données pondérées INEGI Encuesta Intercensal 2015)") +
   scale_y_continuous(labels = comma) +
  theme(plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 12),
        legend.position="bottom",
        plot.subtitle = element_text(size = 9)
        )
  
  ggsave(filename = paste(repertoire,variable,".png", sep = ""), plot = figure,  width = 180, height = 100, units = "mm")
}



# Pré-remplissage de la variable avec les éléments récurrents
pre_remp_diag_bars  <- partial(diag_bars, repertoire = "../images/distri_variables/", labels=libelles_vars)

# Pré-remplissage de la variable avec les éléments récurrents
pre_remp_diag_histo  <- partial(diag_histo, repertoire = "../images/distri_variables/", labels=libelles_vars)
```

```{r}
## CLAVIVP
pre_remp_diag_bars(data,"CLAVIVP","FACTOR_VIV") # appel de la fonction pré-remplie 

## PISOS
pre_remp_diag_bars(data,"PISOS","FACTOR_VIV")

## Combustible
pre_remp_diag_bars(data,"COMBUSTIBLE","FACTOR_VIV")

## ELECTRICIDAD
pre_remp_diag_bars(data,"ELECTRICIDAD","FACTOR_VIV")

## AGUA_ENTUBADA
pre_remp_diag_bars(data,"AGUA_ENTUBADA","FACTOR_VIV")

## ABA_AGUA_ENTU
pre_remp_diag_bars(data,"ABA_AGUA_ENTU","FACTOR_VIV")

## ABA_AGUA_NO_ENTU
pre_remp_diag_bars(data,"ABA_AGUA_NO_ENTU","FACTOR_VIV")

## AIRE_ACON
pre_remp_diag_bars(data,"AIRE_ACON","FACTOR_VIV")

## SERSAN
pre_remp_diag_bars(data,"SERSAN","FACTOR_VIV")

## USOEXC
pre_remp_diag_bars(data,"USOEXC","FACTOR_VIV")

## DRENAJE
pre_remp_diag_bars(data,"DRENAJE","FACTOR_VIV")

## DESTINO_BASURA
pre_remp_diag_bars(data,"DESTINO_BASURA","FACTOR_VIV")

## REFRIGERADOR
pre_remp_diag_bars(data,"REFRIGERADOR","FACTOR_VIV")

##LAVADORA
pre_remp_diag_bars(data,"LAVADORA","FACTOR_VIV")


##HORNO
pre_remp_diag_bars(data,"HORNO","FACTOR_VIV")

##AUTOPROP
pre_remp_diag_bars(data,"AUTOPROP","FACTOR_VIV")

##RADIO
pre_remp_diag_bars(data,"RADIO","FACTOR_VIV")

##R TELEVISOR
pre_remp_diag_bars(data,"TELEVISOR","FACTOR_VIV")

## TELEVISOR_PP
pre_remp_diag_bars(data,"TELEVISOR_PP","FACTOR_VIV")

## COMPUTADORA
pre_remp_diag_bars(data,"COMPUTADORA","FACTOR_VIV")

## TELEFONO
pre_remp_diag_bars(data,"TELEFONO","FACTOR_VIV")

## CELULAR
pre_remp_diag_bars(data,"CELULAR","FACTOR_VIV")

## INTERNET
pre_remp_diag_bars(data,"INTERNET","FACTOR_VIV")

## TENENCIA
pre_remp_diag_bars(data,"TENENCIA","FACTOR_VIV")

## TIPOHOG
pre_remp_diag_bars(data,"TIPOHOG","FACTOR_VIV")


# variable_quantitatives

## TOTCUART 
pre_remp_diag_histo(data,"TOTCUART","FACTOR_VIV")

##CUADORM
pre_remp_diag_histo(data,"CUADORM","FACTOR_VIV")

## NUMPERS
pre_remp_diag_histo(data,"NUMPERS","FACTOR_VIV")

# Variables PERSONA
## PERTE_INDIGENA
pre_remp_diag_bars(data,"PERTE_INDIGENA","FACTOR_PERS")

## HLENGUA
pre_remp_diag_bars(data,"HLENGUA","FACTOR_PERS")

## HESPANOL
pre_remp_diag_bars(data,"HESPANOL","FACTOR_PERS")

## ALFABET
pre_remp_diag_bars(data,"ALFABET","FACTOR_PERS")

## NIVACAD
pre_remp_diag_bars(data,"NIVACAD","FACTOR_PERS")

## CONACT
pre_remp_diag_bars(data,"CONACT","FACTOR_PERS")
```
 [1] "ID_PERSONA"       "ID_VIV"           "ENT"              "NOM_ENT"          "MUN"              "NOM_MUN"          "LOC50K"           "NOM_LOC"         
 [9] "FACTOR_PERS"      "FACTOR_VIV"       "CLAVIVP"          "PISOS"            "TOTCUART"         "CUADORM"          "COMBUSTIBLE"      "ELECTRICIDAD"    
[17] "AGUA_ENTUBADA"    "ABA_AGUA_ENTU"    "ABA_AGUA_NO_ENTU" "AIRE_ACON"        "SERSAN"           "USOEXC"           "DRENAJE"          "DESTINO_BASURA"  
[25] "REFRIGERADOR"     "LAVADORA"         "HORNO"            "AUTOPROP"         "RADIO"            "TELEVISOR"        "TELEVISOR_PP"     "COMPUTADORA"     
[33] "TELEFONO"         "CELULAR"          "INTERNET"         "TENENCIA"         "NUMPERS"          "TIPOHOG"          "PERTE_INDIGENA"   "HLENGUA"         
[41] "HESPANOL"         "NIVACAD"          "ALFABET"          "CONACT"           "CODE_MUN"         "CODE_LOC"
```{r}

variables <- colnames(VIVIENDA_PERSONA_15)
for(variable in variables[12:38]){
  
  if (is.numeric(variable)) {variable <- which(names(VIVIENDA_PERSONA_15) == variable)}
  
  figure <- ggplot(VIVIENDA_PERSONA_15, aes_string(x = variable,fill = variable)) +
  geom_bar(aes(weight = FACTOR_VIV)) +
  labs(title = variable, 
       subtitle = "Zone Urbaine de Mexico 2015",
       x = variable, 
       y = "Nombre d'habitations",
       caption = "(données brutes (non pondérées) INEGI Encuesta Intercensal 2015)") +
    scale_y_continuous(labels = comma) +
  theme(plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 14))
  #figure
  ggsave(filename = paste("../images/ZUM_toutes_variables/2015_ZUM_",variable,".png", sep = ""), plot = figure,  width = 150, height = 150, units = "mm")

}


```

## CLASE DE VIVIENDA PARTICULAR (CLAVIVP)
### Pour l'ensemble de la Zone Urbaine de Mexico

```{r table}
codes <- c(1,2,3,4,5,6,7,8,9,99)

libelle <- c('Casa única en el terreno','Casa que comparte terreno con otra(s)','Casa dúplex, triple o cuádruple','Departamento en edificio','Vivienda en vecindad o cuartería','Cuarto en la azotea de un edificio','Local no construido para habitación','Vivienda móvil','Refugio','No especificado')

traduction <- c('Maison unique sur le terrain',"Maison partageant son terrain avec d'autres",'Casa dúplex, triple o cuádruple','Departamento en edificio','Vivienda en vecindad o cuartería','Cuarto en la azotea de un edificio','Local non prévu pour le logement','Logement mobile','Refuge','Non spécifié')

appelations <- paste(libelle, as.character(codes), sep= " - ")

codification <- cbind(codes, libelle, traduction)
colnames(codification) <- c("Code", "Libellé", "Traduction")
kable(codification, caption = "Classification")
```

```{r CLAVIVP}
figure  <- ggplot(VIVIENDA_PERSONA_15, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
  labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "Zone Urbaine de Mexico 2015",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") + 
       scale_x_discrete(
         breaks=codes, labels =stringr::str_wrap(appelations, 15)) +
       scale_y_continuous(labels = comma) + 
  theme(axis.text.x = element_text(size=8, angle=0)) +
  theme(legend.position="bottom") +
  guides(fill=guide_legend(title="Classes d'habitations\n(CLAVIVP)"))+ 
  coord_flip() +
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 14))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_ZUM2015.png", plot = figure, width = 150, height = 150, units = "mm")
```

![Répartition des classes d'habitations (pondérées) de la Zone Urbaine de Mexico \label{CLAVIVP_ZUM2015}](../images/CLAVIVP/CLAVIVP_ZUM2015.png)


D'après la figure \ref{CLAVIVP_ZUM2015}, à l'échelle de la Zone Urbaine de Mexico, le type d'habitation le plus fréquent est celui de la maison unique sur une parcelle. Viennent ensuite les habitations multiples sur une parcelle et les appartements.


### Par entités fédératives
```{r ENT_facet}
figure  <- ggplot(VIVIENDA_PERSONA_15, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
  labs(title = "Répartition des classes d'habitations \n (pondérées)", 
       subtitle = "Zone Urbaine de Mexico 2015, par entités fédératives",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") + 
       scale_x_discrete(
         breaks=c(1,2,3,4,5,6,7,8,9,99), labels =stringr::str_wrap(appelations, 35)) +
       scale_y_continuous(labels = comma) + 
  theme(axis.text.x = element_text(size=5, angle=0)) +
  theme(legend.position="bottom") +
  guides(fill=guide_legend(title="Classes d'habitations\n(CLAVIVP)"))+ 
  coord_flip() +
  facet_grid(NOM_ENT ~ .) +
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_par_ENT_2015.png", plot = figure, width = 150, height = 170, units = "mm")
```

![Répartition par entités fédératives des classes d'habitations (pondérées) \label{CLAVIVP_par_ENT_2015}](../images/CLAVIVP/CLAVIVP_par_ENT_2015.png)

#### District fédéral
```{r CLAVIVP_Distrito_Federal}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'Distrito Federal')
figure <- ggplot(data_ENT, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "District fédéral 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_district_fed_par_MUN_2015.png", plot = figure, width = 150, height = 170, units = "mm")
  
```

![Répartition  des classes d'habitations (pondérées) au sein du district fédéral \label{CLAVIVP_district_fed_par_MUN_2015}](../images/CLAVIVP/CLAVIVP_district_fed_par_MUN_2015.png)

#### México
```{r CLAVIVP_México}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'México')
mun_mexico <- unique(data_ENT$NOM_MUN)

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[1:12])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part1.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[13:24])

figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part2.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[25:36])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
    facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part3.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[37:48])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part4.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[49:60])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
    facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part5.png", plot = figure, width = 150, height = 170, units = "mm")
```


![Répartition  des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part1}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part1.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part2}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part2.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part3}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part3.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part4}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part4.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part5}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part5.png)


#### Hidalgo

```{r CLAVIVP_Hidalgo}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'Hidalgo')
figure <- ggplot(data_ENT, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "Hidalgo 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN)+
  theme(plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_hidalgo_par_MUN_2015.png", plot = figure, width = 150, height = 170, units = "mm")
  
```

![Répartition des classes d'habitations (pondérées) au sein d'Hidalgo \label{CLAVIVP_hidalgo_par_MUN_2015}](../images/CLAVIVP/CLAVIVP_hidalgo_par_MUN_2015.png)
```{r CLAVIVP_nettoyage}
rm(d, data, data_ENT, data_Mexico, figure, appelations, codes, libelle, mun_mexico, traduction)
```
## Electrcidad

### Pour l'ensemble de la Zone Urbaine de Mexico

```{r table}
codes <- c(5,7,9)

libelle <- c('Si','No','No especificado')

traduction <- c('Oui',"Non",'Non spécifié')

appelations <- paste(libelle, as.character(codes), sep= " - ")

codification <- cbind(codes, libelle, traduction)
colnames(codification) <- c("Code", "Libellé", "Traduction")
kable(codification, caption = "Classification")
```

```{r CLAVIVP}
figure  <- ggplot(VIVIENDA_PERSONA_15, aes(ELECTRICIDAD)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= ELECTRICIDAD)) + 
  labs(title = "Accès à l'électricité (pondérés)", 
       subtitle = "Zone Urbaine de Mexico 2015",
       x = "Accès à l'électricité", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") + 
       scale_x_discrete(
         breaks=codes, labels =stringr::str_wrap(appelations, 15)) +
       scale_y_continuous(labels = comma) + 
  theme(axis.text.x = element_text(size=8, angle=0)) +
  theme(legend.position="bottom") +
  guides(fill=guide_legend(title="Accès à l'électricité \n(ELECTICIDAD)"))+ 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 14))
ggsave(filename = "../images/ELECTRICIDAD/ELECTICIDAD_ZUM2015.png", plot = figure, width = 150, height = 150, units = "mm")
```

![Répartition de l'accès à l'électrcité (pondérées) de la Zone Urbaine de Mexico \label{ELECTRICIDAD_ZUM2015}](../images/ELECTRICIDAD/ELECTRICIDAD_ZUM2015.png)


D'après la figure \ref{CLAVIVP_ZUM2015}, à l'échelle de la Zone Urbaine de Mexico, le type d'habitation le plus fréquent est celui de la maison unique sur une parcelle. Viennent ensuite les habitations multiples sur une parcelle et les appartements.


### Par entités fédératives
```{r ENT_facet}
figure  <- ggplot(VIVIENDA_PERSONA_15, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
  labs(title = "Répartition des classes d'habitations \n (pondérées)", 
       subtitle = "Zone Urbaine de Mexico 2015, par entités fédératives",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") + 
       scale_x_discrete(
         breaks=c(1,2,3,4,5,6,7,8,9,99), labels =stringr::str_wrap(appelations, 35)) +
       scale_y_continuous(labels = comma) + 
  theme(axis.text.x = element_text(size=5, angle=0)) +
  theme(legend.position="bottom") +
  guides(fill=guide_legend(title="Classes d'habitations\n(CLAVIVP)"))+ 
  coord_flip() +
  facet_grid(NOM_ENT ~ .) +
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_par_ENT_2015.png", plot = figure, width = 150, height = 170, units = "mm")
```

![Répartition par entités fédératives des classes d'habitations (pondérées) \label{CLAVIVP_par_ENT_2015}](../images/CLAVIVP/CLAVIVP_par_ENT_2015.png)

#### District fédéral
```{r CLAVIVP_Distrito_Federal}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'Distrito Federal')
figure <- ggplot(data_ENT, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "District fédéral 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_district_fed_par_MUN_2015.png", plot = figure, width = 150, height = 170, units = "mm")
  
```

![Répartition  des classes d'habitations (pondérées) au sein du district fédéral \label{CLAVIVP_district_fed_par_MUN_2015}](../images/CLAVIVP/CLAVIVP_district_fed_par_MUN_2015.png)

#### México
```{r CLAVIVP_México}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'México')
mun_mexico <- unique(data_ENT$NOM_MUN)

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[1:12])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part1.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[13:24])

figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part2.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[25:36])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
    facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part3.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[37:48])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part4.png", plot = figure, width = 150, height = 170, units = "mm")

data_Mexico <- filter(data_ENT, NOM_MUN %in% mun_mexico[49:60])
figure <- ggplot(data_Mexico, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "México 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
    facet_wrap(~NOM_MUN) + 
  theme(legend.position="none", plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part5.png", plot = figure, width = 150, height = 170, units = "mm")
```


![Répartition  des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part1}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part1.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part2}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part2.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part3}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part3.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part4}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part4.png)

![Répartition des classes d'habitations (pondérées) au sein de Mexico \label{CLAVIVP_mexico_par_MUN_2015_part5}](../images/CLAVIVP/CLAVIVP_mexico_par_MUN_2015_part5.png)


#### Hidalgo

```{r CLAVIVP_Hidalgo}
data_ENT <- filter(VIVIENDA_PERSONA_15, NOM_ENT == 'Hidalgo')
figure <- ggplot(data_ENT, aes(CLAVIVP)) +
  geom_bar(aes(weight = FACTOR_VIV, fill= CLAVIVP)) + 
    labs(title = "Répartition des classes d'habitations (pondérées)", 
       subtitle = "Hidalgo 2015, par Municipe",
       x = "Classes d'habitations", 
       y = "Nombre d'habitations",
       caption = "(données INEGI Encuesta Intercensal 2015)") +
  scale_y_continuous(labels = comma) + 
  facet_wrap(~NOM_MUN)+
  theme(plot.title = element_text(family = 'Helvetica', 
                              color = '#666666', 
                              face = 'bold', 
                              size = 13))
ggsave(filename = "../images/CLAVIVP/CLAVIVP_hidalgo_par_MUN_2015.png", plot = figure, width = 150, height = 170, units = "mm")
  
```

![Répartition des classes d'habitations (pondérées) au sein d'Hidalgo \label{CLAVIVP_hidalgo_par_MUN_2015}](../images/CLAVIVP/CLAVIVP_hidalgo_par_MUN_2015.png)
```{r CLAVIVP_nettoyage}
rm(d, data, data_ENT, data_Mexico, figure, appelations, codes, libelle, mun_mexico, traduction)
```